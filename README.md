# Reach Robotics Game Jam AR Demo Code

This repo contains a few scenes to demo Unity's AR Foundation code for rapidly setting up an Augmented Reality game.

It serves as an accompaniment to the workshop given by Reach Robotics at the Catalyst Hack 2019.

Note that the content in these scenes is based heavily (in some cases, verbatim) on the code from the [AR Foundation Samples repository](https://github.com/Unity-Technologies/arfoundation-samples).

For more detailed information on the inner workings of AR Foundation, we suggest looking through the [package manual on the Unity website](https://docs.unity3d.com/Packages/com.unity.xr.arfoundation@1.0/).


## Basic
A simple scene containing the basic AR Foundation elements, and a simple script to drop a prefab onto a plane.

## Mock
This contains an additional scene to illustrate how you might use the alpha release of the XR Mock package to test your game in-editor before pushing it to the phone.

These basically involve switching your Unity Package repository over to staging and adding the mock package manually.

Note that there are some issues with XR Mock at the current time which prevent compilation iOS. The fix is to modify

`Library/PackageCache/com.unity.xr.mock@0.0.1-preview.14/com.unity.xr.remoting/Editor/EditorRemoting.Utils.cs`

Line 47, change to: 

`var ipAddress = remoteAddress.ip;`

You can then create mocked trackables and control camera movement using the scripts in the "Mock" directory.

Note that these are basica